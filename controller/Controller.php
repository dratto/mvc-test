<?php

namespace Controller;

class Controller
{

    protected $view;


    public function view($view_name)
    {    
      include __DIR__."/../view/$view_name.php";
    }
}
