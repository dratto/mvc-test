<?php

namespace Controller;

require_once __DIR__.'/Controller.php';
require_once __DIR__.'/../model/Cadastro.php';


class CadastroController extends Controller
{

  public function index()
  {
    $cadastro = new \Model\Cadastro();
    $cadastro->insert(['nome' => 'Diogo Borges Dias Ratto', 'idade' => '20', 'telefone' => '(11) 96666-6666' ]);
    $this->view('index');
  }

  public function create()
  {
    $this->view('create');
  }

  public function store($data)
  {
    $cadastro = new Model\Cadastro();
    $cadastro->insert(['nome' => $data['nome'], 'idade' => $data['idade'], 'telefone' => $data['telefone'] ]);
  }

  public function edit()
  {
    $this->view('edit');
  }

  public function update()
  {
    //   TODO
  }

  public function delete()
  {
    //  TODO
  }



}
