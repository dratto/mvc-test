<?php
ini_set('display_errors', 1);

if(!isset($_POST)) {
    header('Location: /index.php');
}
require_once __DIR__.'/../controller/CadastroController.php';

$cadastro = new Controller\CadastroController;
$cadastro->store();
