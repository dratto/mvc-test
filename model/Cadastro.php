<?php

namespace Model;

require_once __DIR__ . '/Model.php';

class Cadastro extends Model
{
   public function __construct()
   {
       $this->table = 'cadastro';
       parent::__construct();
   }
}
