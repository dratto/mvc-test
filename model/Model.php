<?php

namespace Model;

require_once __DIR__. '/../config/database.php';

class Model
{

  private $pdo;

  protected $table;

  public function __construct()
  {
      $dsn = DB_DRIVER .':host=' . DB_HOST . ';dbname=' . DB_NAME;
      $this->pdo = new \PDO($dsn, DB_USER, DB_PASS);
  }

  public function insert($fields_and_values)
  {
    $fields      = [];
    $fake_values = [];
    $values      = [];
    foreach($fields_and_values as $f => $v) {
      $fields     []= $f;
      $fake_values[]= ':'.$f;
      $values     []= $v;
    }
    $fields      = implode(',', $fields);
    $fake_values_string = implode(',', $fake_values);
    $query = "INSERT INTO  {$this->table} ($fields) VALUES ($fake_values_string)";
    $stmt = $this->pdo->prepare($query);
    $max = count($values);
    for($i = 0 ; $i < $max ; ++$i) {
      $stmt->bindValue($fake_values[$i], $values[$i]);
    }
    return $stmt->execute();
  }

  public function update()
  {

  }

  public function destroy($where_field)
  {
    $stmt = $this->pdo->query("DELETE FROM {$this->table} WHERE $where_field");
    return $stmt->execute();
  }

}
